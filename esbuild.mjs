import esbuild from 'esbuild'
import fs from 'fs'
import toml from 'toml'
import path from 'path'
import { execSync } from 'child_process'

const production = process.argv.includes('--production')
const watch = process.argv.includes('--watch')

function buildJSON() {
	const createLangConfig = () => ({
		comments: {
			lineComment: '//',
			blockComment: ['/*', '*/']
		},
		brackets: [
			['{', '}'],
			['[', ']'],
			['(', ')']
		],
		autoClosingPairs: [
			['{', '}'],
			['[', ']'],
			['(', ')'],
			['"', '"'],
			["'", "'"],
			['<', '>']
		],
		surroundingPairs: [
			['{', '}'],
			['[', ']'],
			['(', ')'],
			['"', '"'],
			["'", "'"],
			['<', '>']
		]
	})

	const snippetsTOML = 'src/toml/snippets.toml'
	const snippetsJSON = 'dist/json/snippets.json'
	const grammarTOML = 'src/toml/Grammar.tmLanguage.toml'
	const grammarJSON = 'dist/json/Grammar.tmLanguage.json'
	const langConfigJSON = 'dist/json/langConfig.json'
	const langConfig = createLangConfig()

	const jsonDir = path.dirname(snippetsJSON)
	if (!fs.existsSync(jsonDir)) {
		fs.mkdirSync(jsonDir, { recursive: true })
	}

	try {
		const snippetsJsonContent = toml.parse(fs.readFileSync(snippetsTOML, 'utf8'))
		const grammarJsonContent = toml.parse(fs.readFileSync(grammarTOML, 'utf8'))

		fs.writeFileSync(snippetsJSON, JSON.stringify(snippetsJsonContent, null, 0))
		fs.writeFileSync(grammarJSON, JSON.stringify(grammarJsonContent, null, 0))
		fs.writeFileSync(langConfigJSON, JSON.stringify(langConfig, null, 0))
	} catch (error) {
		console.error('Error processing files:', error)
	}
}

async function main() {
	const ctx = await esbuild.context({
		entryPoints: ['src/extension.ts'],
		bundle: true,
		format: 'cjs',
		minify: production,
		metafile: !production,
		sourcemap: !production,
		sourcesContent: false,
		platform: 'node',
		// outfile: 'dist/extension.js',
		outdir: 'dist',
		external: ['vscode'],
		logLevel: 'silent',
		plugins: [
			/* add to the end of plugins array */
			esbuildProblemMatcherPlugin
		]
	})

	if (watch) {
		await ctx.watch()
		console.log('Extension build started in watch mode')
	} else {
		const startTime = Date.now()
		await ctx.rebuild()
		await ctx.dispose()
		execSync('tsc --declaration --emitDeclarationOnly --outDir dist/types')
		console.log('TypeScript declaration files generated')
		buildJSON()
		execSync('bun extractTypes.ts', { stdio: 'inherit' })
		console.log('JSON files generated')
		console.log(`Build Completed. Total build time: ${Date.now() - startTime}ms (${(Date.now() - startTime) / 1000}s)`)
	}
}

/**
 * @type {import('esbuild').Plugin}
 */
const esbuildProblemMatcherPlugin = {
	name: 'esbuild-problem-matcher',
	setup(build) {
		build.onStart(() => {
			// if (fs.existsSync('dist')) {
			// 	fs.rmSync('dist', { recursive: true })
			// }
			console.log('Extension Build Started...')
		})
		build.onEnd((result) => {
			if (result.errors.length > 0) {
				console.error(`Build completed with ${result.errors.length} error(s):`)
				result.errors.forEach(({ text, location }) => {
					console.error(`✘ [ERROR] ${text}`)
					if (location) {
						console.error(`${location.file}:${location.line}:${location.column}`)
					}
				})
			} else {
				console.log('Extension built successfully.')
			}
		})
	}
}

main().catch((e) => {
	console.error(e)
	process.exit(1)
})
