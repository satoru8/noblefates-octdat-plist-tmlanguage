# Change Log

All notable changes to the "nf octdat" extension will be documented here.

## Updates

### 0.4.2
- 

### 0.4.1
- Require VSCode 1.93.0+
- Update toolips.
- Add new command `createOctModInfo`. Right click in an octdat, use Ctrl+K 9, or from command palette.
- Fix issue in syntax highlighting where certain incorrect types/inherits/alias were highlighted as IDs.


### 0.4.0
- Refactor and cleanup code.
- Fix remaining formatter issues.
- Add new tooltips, currently includes `id, type, alias, inherit, appendonly, abstract, $clear, list modifiers`.
- Add new CompletionItemProvider (IntelliSense) for `type`. Includes 873 unique types from the game's octdats.
- New settings for `useTooltips` and `indentation`.
* Indentation setting is for the main formatter. Basic Format octdat command does not use this, and is a backup.

### 0.3.8
- Fix settings toggle(oops).
- Remove test msg.

### 0.3.7
- Add settings for `formatOnSave` and `saveOnFormat`.
- Rename `Basic Format Octdat` command.
- Use TS, ESM & compile code.
- Reduce size. Total 60kb~

### 0.3.6
- Skip over comments.
- Add color support for 'null'.
- Fix overindenting in specific cases with grammar and arrays containing objects or arrays.
- New context menu items "Format .octdat Files in Directory" & "Format Octdat Test". The latter is a simplified version of the formatter.
- Remove NF themes. Highlighting should work across all themes using standard tokens.
- Updated readme.

### 0.3.5
- Add Format Directory command.
- Fix some issues with grammar formatting.

### 0.3.4
- Cleanup repo.
- Some changes to formatting logic - test array format, fix `snippets.json` syntax errors, switch to custom theme.

### 0.3.3
- Add support for replace and edit of list items.
- Clean up formatter a bit.

### 0.3.2
- Update formatter.

### 0.3.1
- Package update.

### 0.3.0
- Add formatter.

### 0.2.6
- Added support for `abstract`, `appendonly`, `alias`, `$clear`.
- Fixed some colors in NFDark.
- Removed old `src` folder.
- Cleaned up & organized Grammar.

### 0.2.4
- Fixed some snippets.

### 0.2.2
- Fix some typos and mistakes in snippets.

### 0.2.1
- Lots of new stuff: NF Dark theme, snippets, fixes, and QoL improvements. Refer to the readme.

### 0.1.6
- Fix tab stop in recipe snippet.

### 0.1.5
- Added new snippets and keybinds.

### 0.1.4
- New keybinds, new snippets. Check Feature Contributions tab for more info.

### 0.1.3
- Added auto-closing support for `<>`.

### 0.1.2
- Added support for numbers within IDs.

### 0.1.1
- Fix for Types not working.

### 0.1.0
- N/A

### 0.0.7
- Fixed typo.
- Removed useless files.

### 0.0.6
- Fixed brackets.

### 0.0.5
- Fixed coloring of `=`. They won't be purple anymore.

### 0.0.4
- Switch to `tmLanguage`.
- `.plist` saved for reference.

### 0.0.3
- Fixes and cleanup.

### 0.0.2
- Initial release.

### 0.0.1
- rawwr?
