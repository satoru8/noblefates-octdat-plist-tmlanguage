import * as vscode from 'vscode'
import { join } from 'path'

function loadTypes() {
	const typesPath = join(__dirname, 'json/types.json')
	return require(typesPath) as string[]
}

class OctdatCompletionProvider implements vscode.CompletionItemProvider {
	private types: string[] = loadTypes()
	private static readonly completionItemsCache = new Map<string, vscode.CompletionItem[]>()

	constructor() {
		OctdatCompletionProvider.completionItemsCache.set('type', this.createCompletionItems())
	}

	private createCompletionItems(): vscode.CompletionItem[] {
		const completionItems = this.types.map((type) => {
			const item = new vscode.CompletionItem(type, vscode.CompletionItemKind.Class)
			item.insertText = new vscode.SnippetString(`${type}`)
			item.filterText = type
			item.detail = 'Octdat Type'
			item.documentation = `This is a type used in Octdat scripting: ${type}`
			return item
		})
		return completionItems
	}

	public provideCompletionItems(
		document: vscode.TextDocument,
		position: vscode.Position,
		token: vscode.CancellationToken,
		context: vscode.CompletionContext
	): vscode.CompletionItem[] {
		if (token.isCancellationRequested) {
			return []
		}

		const line = document.lineAt(position)
		const linePrefix = line.text.slice(0, position.character).trim()

		const items = (OctdatCompletionProvider.completionItemsCache.get('type') || []).sort((a, b) =>
			(a.label as string).localeCompare(b.label as string)
		)

		if (linePrefix.startsWith('type ')) {
			const prefix = linePrefix.slice('type '.length).toLowerCase()
			return this.filterAndMapItems(items, prefix, false)
		} else if (linePrefix === 'type') {
			return this.filterAndMapItems(items, '', true)
		}

		return []
	}

	private filterAndMapItems(
		items: vscode.CompletionItem[],
		prefix: string,
		includeTypePrefix: boolean
	): vscode.CompletionItem[] {
		return items
			.filter((item) => (item.label as string).toLowerCase().startsWith(prefix))
			.map((item) => {
				item.insertText = new vscode.SnippetString(
					includeTypePrefix ? `type ${item.label as string}` : `${item.label as string}`
				)
				return item
			})
	}
}

export { OctdatCompletionProvider }
