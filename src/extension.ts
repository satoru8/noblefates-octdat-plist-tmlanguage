import { formatOctDat } from './formatting'
import { formatOctDatBasic } from './formattingBasic'
import { getTooltip } from './tooltips'
import { ExtensionSettings } from './settings'
import { registerCommands } from './commands'
import { OctdatCompletionProvider } from './octdatIntelliSense'
import * as vscode from 'vscode'
import { join } from 'path'
import fs from 'fs'

export function activate(context: vscode.ExtensionContext) {
	// console.log('[NF-OCT] OctDat Extension Activated')
	ExtensionSettings.initialize()

	registerCommands(context)

	let hoverProvider: vscode.Disposable | undefined
	let formatOnSaveDisposable: vscode.Disposable | undefined

	const createRange = (document: vscode.TextDocument) =>
		new vscode.Range(
			new vscode.Position(0, 0),
			new vscode.Position(document.lineCount - 1, document.lineAt(document.lineCount - 1).text.length)
		)

	const updateFeatureToggles = () => {
		const useTooltips = ExtensionSettings.get('useTooltips', false)
		const formatOnSave = ExtensionSettings.get('formatOnSave', false)

		if (useTooltips) {
			if (!hoverProvider) {
				hoverProvider = vscode.languages.registerHoverProvider('octdat', {
					provideHover(document: vscode.TextDocument, position: vscode.Position) {
						const word = getWordAtPosition(document, position)
						// console.log(`[NF-OCT] Hovered word: ${word}`)
						const tooltip = getTooltip(word)
						return tooltip ? new vscode.Hover(tooltip) : null
					}
				})
				context.subscriptions.push(hoverProvider)
			}
		} else {
			hoverProvider?.dispose()
			hoverProvider = undefined
		}

		if (formatOnSave) {
			if (!formatOnSaveDisposable) {
				formatOnSaveDisposable = vscode.workspace.onWillSaveTextDocument((event) => {
					handleFormatOnSave(event.document)
				})
				context.subscriptions.push(formatOnSaveDisposable)
			}
		} else {
			formatOnSaveDisposable?.dispose()
			formatOnSaveDisposable = undefined
		}
	}

	const handleFormatOnSave = async (document: vscode.TextDocument) => {
		if (document.languageId === 'octdat') {
			const edits = formatOctDat(document, createRange(document))
			const workspaceEdit = new vscode.WorkspaceEdit()
			workspaceEdit.set(document.uri, edits)

			const success = await vscode.workspace.applyEdit(workspaceEdit)
			if (success) {
				await document.save()
			} else {
				vscode.window.showWarningMessage('Formatting edits were not applied successfully.')
			}
		}
	}

	const registerFormatOnSaveHandler = () => {
		formatOnSaveDisposable?.dispose()

		if (ExtensionSettings.get('formatOnSave', false)) {
			formatOnSaveDisposable = vscode.workspace.onWillSaveTextDocument(async (event) => {
				await handleFormatOnSave(event.document)
			})
			context.subscriptions.push(formatOnSaveDisposable)
		} else {
			formatOnSaveDisposable = undefined
		}
	}

	updateFeatureToggles()

	ExtensionSettings.onChange(() => {
		console.log('[NF-OCT] Configuration changed, updating feature toggles...')
		updateFeatureToggles()
		registerFormatOnSaveHandler()
	})

	context.subscriptions.push(
		vscode.languages.registerDocumentFormattingEditProvider('octdat', {
			provideDocumentFormattingEdits: async (document) => {
				try {
					if (document.lineCount === 0) return []

					const edits = formatOctDat(document, createRange(document))
					const saveOnFormat = ExtensionSettings.get('saveOnFormat', false)

					await applyEdits(document, edits, saveOnFormat)
					return edits
				} catch (error: any) {
					handleError(error)
					return []
				}
			}
		})
	)

	context.subscriptions.push(
		vscode.commands.registerCommand('extension.formatBasic', async () => {
			const editor = vscode.window.activeTextEditor
			if (editor?.document.languageId === 'octdat') {
				try {
					const document = editor.document
					const edits = formatOctDatBasic(document, createRange(document))
					const saveOnFormat = ExtensionSettings.get('saveOnFormat', false)

					await applyEdits(document, edits, saveOnFormat)
				} catch (error: any) {
					handleError(error)
				}
			} else {
				vscode.window.showWarningMessage('No active OctDat file to format.')
			}
		})
	)

	context.subscriptions.push(
		vscode.commands.registerCommand('extension.formatOctdatDir', async () => {
			const folderUri = await vscode.window.showOpenDialog({
				canSelectFolders: true,
				canSelectFiles: false,
				canSelectMany: false
			})

			if (folderUri?.[0]) {
				const folderPath = folderUri[0].fsPath
				const files = findAllOctdatFiles(folderPath)

				for (const file of files) {
					const document = await vscode.workspace.openTextDocument(file)
					const edits = formatOctDat(document, createRange(document))
					const edit = new vscode.WorkspaceEdit()
					edit.set(document.uri, edits)
					await vscode.workspace.applyEdit(edit)
					await document.save()
				}

				vscode.window.showInformationMessage(
					`Formatted ${files.length} .octdat files in ${folderPath} and its subdirectories`
				)
			}
		})
	)

	const octdatCompletionProvider = new OctdatCompletionProvider()
	context.subscriptions.push(vscode.languages.registerCompletionItemProvider('octdat', octdatCompletionProvider))
}

function getWordAtPosition(document: vscode.TextDocument, position: vscode.Position) {
	const line = document.lineAt(position.line)
	const wordRange = line.range
	return line.text.substring(wordRange.start.character, wordRange.end.character).trim()
}

function findAllOctdatFiles(dir: string): string[] {
	let results: string[] = []
	const list = fs.readdirSync(dir)
	list.forEach((file) => {
		const filePath = join(dir, file)
		const stat = fs.statSync(filePath)
		if (stat.isDirectory()) {
			results = results.concat(findAllOctdatFiles(filePath))
		} else if (file.endsWith('.octdat')) {
			results.push(filePath)
		}
	})
	return results
}

async function applyEdits(document: vscode.TextDocument, edits: vscode.TextEdit[], saveOnFormat: boolean) {
	if (edits.length > 0) {
		const edit = new vscode.WorkspaceEdit()
		edit.set(document.uri, edits)
		await vscode.workspace.applyEdit(edit)

		if (saveOnFormat) {
			await document.save()
		}
	}
}

function handleError(error: any) {
	console.error(`[NF-OCT] Error formatting document: ${error.message}`)
	vscode.window.showErrorMessage('Error formatting OctDat document. See output channel for details.')
}
