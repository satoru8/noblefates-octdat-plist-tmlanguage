import * as vscode from 'vscode'
import { join } from 'path'
import { promises as fs } from 'fs'

export function registerCommands(context: vscode.ExtensionContext) {
	const createOctModInfo = vscode.commands.registerCommand('extension.createOctModInfo', async () => {
		const workspaceFolders = vscode.workspace.workspaceFolders
		const mainTitle = 'Create OctMod.info'

		if (!workspaceFolders) {
			vscode.window.showErrorMessage('No workspace folder is open.')
			return
		}

		let workspaceRoot: string
		if (workspaceFolders.length === 1) {
			workspaceRoot = workspaceFolders[0].uri.fsPath
		} else {
			const folderOptions = workspaceFolders.map((folder) => folder.name)
			const selectedFolder = await vscode.window.showQuickPick(folderOptions, {
				title: mainTitle,
				placeHolder: 'Select the folder where you want to create the OctMod.info file'
			})
			if (!selectedFolder) return vscode.window.showErrorMessage('You need to select a folder.')

			const folder = workspaceFolders.find((f) => f.name === selectedFolder)
			workspaceRoot = folder?.uri.fsPath || workspaceFolders[0].uri.fsPath
		}

		let currentFolder = workspaceRoot

		while (true) {
			const subfolders = await getSubfolders(currentFolder)

			if (subfolders.length === 0) break

			const selectedSubfolder = await vscode.window.showQuickPick(subfolders, {
				title: mainTitle,
				placeHolder: 'Select a subfolder or press escape to use the selected folder'
			})

			if (selectedSubfolder) {
				currentFolder = join(currentFolder, selectedSubfolder)
			} else {
				break
			}
		}

		const modName = await vscode.window.showInputBox({
			title: mainTitle,
			prompt: 'Name of the mod',
			placeHolder: 'ModName',
			value: 'ModName',
			validateInput: (value) => (value ? undefined : 'Please enter a mod name.')
		})
		if (!modName) return vscode.window.showErrorMessage('Mod name is required.')

		const previewImage = await vscode.window.showInputBox({
			title: mainTitle,
			prompt: 'Enter the preview image file name',
			placeHolder: 'Preview.png',
			value: 'Preview.png',
			validateInput: (value) => {
				const regex = /\.(png|jpg|jpeg|gif|webp)$/i
				if (value === '') return
				return regex.test(value) ? undefined : 'Please enter a valid image file name (.png, .jpg, .jpeg, .gif, .webp).'
			}
		})
		if (!previewImage) return vscode.window.showErrorMessage('Preview image is required.')

		const description = await vscode.window.showInputBox({
			title: mainTitle,
			prompt: 'Enter the description',
			placeHolder: 'Optional. Press enter to skip.'
		})

		const compatibleVersions = await vscode.window.showInputBox({
			title: mainTitle,
			prompt: 'Enter the compatible versions',
			placeHolder: 'Optional. Press enter to skip. Example: v0.26;v0.27',
			validateInput: (value) => {
				const regex = /^v\d+\.\d+(;v\d+\.\d+)*;?$/i
				if (value === '') return
				return regex.test(value) ? undefined : 'Please enter a valid compatible version in the format v0.26;v0.27'
			}
		})

		const compatibilityPatches = await vscode.window.showInputBox({
			title: mainTitle,
			prompt: 'Enter the compatibility patches',
			placeHolder: 'Optional. Press enter to skip. Example: 2786247438:2789556010',
			validateInput: (value) => {
				const regex = /^(\d+):(\d+)$/
				if (value === '') return
				return regex.test(value)
					? undefined
					: 'Please enter a valid compatibility patch in the format 2786247438:2789556010'
			}
		})

		const localize = await vscode.window.showQuickPick(['true', 'false'], {
			title: mainTitle,
			placeHolder: 'Optional. Press Escape to skip.'
		})

		const defaultOrder = await vscode.window.showInputBox({
			title: mainTitle,
			prompt: 'Enter the default order',
			placeHolder: 'Optional. Press enter to skip. Example: 50'
		})

		const octModFilePath = join(currentFolder, 'OctMod.info')
		const fileContent =
			`name=${modName}\npreview=${previewImage}\n` +
			(description ? `description=${description}\n` : '') +
			(defaultOrder ? `defaultOrder=${defaultOrder}\n` : '') +
			(compatibleVersions ? `compatibleVersions=${compatibleVersions}\n` : '') +
			(compatibilityPatches ? `compatibilityPatches=${compatibilityPatches}\n` : '') +
			(localize ? `localize=${localize}\n` : '')

		try {
			await fs.writeFile(octModFilePath, fileContent, 'utf8')
			vscode.window.showInformationMessage('OctMod.info file created successfully!')
		} catch (err: any) {
			vscode.window.showErrorMessage(`Error creating file: ${err.message}`)
		}
	})

	context.subscriptions.push(createOctModInfo)
}

async function getSubfolders(folderPath: string): Promise<string[]> {
	try {
		const files = await fs.readdir(folderPath, { withFileTypes: true })
		return files.filter((file) => file.isDirectory()).map((dir) => dir.name)
	} catch (err: any) {
		vscode.window.showErrorMessage(`Error reading subfolders: ${err.message}`)
		return []
	}
}
