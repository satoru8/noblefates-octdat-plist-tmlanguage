import * as vscode from 'vscode'

export class ExtensionSettings {
	private static config: vscode.WorkspaceConfiguration | undefined

	public static initialize() {
		try {
			this.config = vscode.workspace.getConfiguration('octdat')
			console.log('[NF-OCT] Configuration initialized:', this.config)
		} catch (error) {
			console.error('[NF-OCT] Error initializing configuration:', error)
		}
	}

	public static get<T>(key: string, defaultValue: T): T {
		if (!this.config) {
			console.warn('[NF-OCT] Configuration not initialized.')
			return defaultValue
		}

		try {
			const value = this.config.get<T>(key, defaultValue)
			// console.log(`[NF-OCT] Retrieved setting ${key}:`, value)
			return value
		} catch (error) {
			console.error(`[NF-OCT] Error retrieving setting ${key}:`, error)
			return defaultValue
		}
	}

	public static onChange(callback: () => void) {
		if (!this.config) {
			console.warn('[NF-OCT] Configuration not initialized.')
			return
		}

		const handleChange = (event: vscode.ConfigurationChangeEvent) => {
			if (event.affectsConfiguration('octdat')) {
				try {
					this.config = vscode.workspace.getConfiguration('octdat')
					// console.log('[NF-OCT] Configuration changed')
					callback()
				} catch (error) {
					console.error('[NF-OCT] Error handling configuration change:', error)
				}
			}
		}

		vscode.workspace.onDidChangeConfiguration(handleChange)
	}
}
