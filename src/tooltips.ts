import * as vscode from 'vscode'

const TOOLTIP_ID = new vscode.MarkdownString(
	'**id**: The unique ID assigned to the object.\n\n' +
	'**Example**: \n```\nid Fantasy.StatusEffects.Paths.Dirt\n```\n\n' +
	'**Note**: Ensure the ID is unique within the context to avoid conflicts. Using your mod name as the base will help ensure uniqueness.'
)

const TOOLTIP_TYPE = new vscode.MarkdownString(
	"**type**: Specifies the type of the object. This property is linked to the C# class name used in the game's code, determining the object's behavior and attributes.\n\n" +
	'**Example**: \n```\nid Oct.Items.Armor.Leather\ntype InstancedItemType\n```\n\n' +
	'**Related**: '
)

const TOOLTIP_ALIAS = new vscode.MarkdownString(
	"**alias**: Defines an alternative reference name for the object. During the game's load process, any reference to the alias is replaced with the corresponding ID.\n\n" +
	'**Example**: \n```octdat\n' +
	'{\n' +
	'  id Alchemy.Items.Gems.Building.T3\n' +
	'  type InstancedItemType\n' +
	'  inherit Alchemy.Items.Gems.Base\n' +
	'  alias Gem.Items.BuildingGem.T3\n' +
	'}\n' +
	'```\n\n' +
	'**Tip**: Avoid using common words as aliases to prevent unintended replacements.'
)

const TOOLTIP_INHERIT = new vscode.MarkdownString(
	'**inherit**: Allows an object to use properties from another ID(object). Adding overlapping properties to an ID will override the properties of the inherited ID.\n\n' +
	'**Example**: \n```\nid Oct.Biomes.Temperate.Forest\ntype BiomeType\ninherit Oct.Biomes.Temperate\n```\n\n' +
	'**Tip**: Use with `abstract` to create an object template that can be inherited from.'
)

const TOOLTIP_APPENDONLY = new vscode.MarkdownString(
	'**appendonly**: A flag indicating that the object definition is intended to update an existing object rather than create a new one. ' +
	'This is particularly useful for mods that aim to enhance or modify objects from other mods, facilitating compatibility and cross-mod support.\n\n' +
	'**Example**: \n```octdat\n' +
	'{\n' +
	'  id Oct.Settings.Game.RabidDifficulty\n' +
	'  type SliderSettingDefinition\n' +
	'  appendonly\n' +
	'  min = 0\n' +
	'  max = 10\n' +
	'  step = .1\n' +
	'}\n' +
	'```\n'
)

const TOOLTIP_ABSTRACT = new vscode.MarkdownString(
	'**abstract**: Used to declare a base object. This keyword sets the ID to be inheritable only, serving as a template for other objects.\n\n' +
	'**Example**: \n```octdat\n' +
	'{\n' +
	'  id Alchemy.Recipes.Ring.Enchanted.Base\n' +
	'  type ItemRecipe\n' +
	'  abstract\n' +
	'  commandType = CraftItemCommand\n' +
	'  progressMultiplier = .435\n' +
	'  researchEntry = <Alchemy.Research.Ring.Enchanted>\n' +
	'  craftingLoop = ClothCraftingLoop\n' +
	'  qualityDifficulty = 1\n' +
	'  skillCurve = .75\n' +
	'  craftAt = \n' +
	'  [\n' +
	'    <Alchemy.AlchemyTable>\n' +
	'  ]\n' +
	'}\n' +
	'```\n\n' +
	'**Related**: Use with `inherit` to add new properties or behaviors to an existing object.\n' +
	'**Note**: Abstract objects cannot be instantiated directly.'
)

const TOOLTIP_CLEAR = new vscode.MarkdownString(
	"**$clear**: A keyword to clear an array list within the object's definition. This can be used to reset or remove default elements before adding new ones, ensuring a clean slate for modifications.\n\n" +
	'**Example**: \n' +
	'```\n' +
	'{\n' +
	'    id Oct.Characters.Humanoids.Elf\n' +
	'    type CharacterType\n\n' +
	'    attributePotentialDeltas = \n' +
	'    [\n' +
	'        $clear\n' +
	'        {\n' +
	'            type CharacterTypeAttributePotentialDelta\n' +
	'            attribute = Ranged\n' +
	'            delta = 1\n' +
	'        }\n' +
	'        {\n' +
	'            type CharacterTypeAttributePotentialDelta\n' +
	'            attribute = Foraging\n' +
	'            delta = 1\n' +
	'        }\n' +
	'    ]\n' +
	'}\n' +
	'```\n\n' +
	'**Tip**: Use before adding new entries to avoid duplications.'
)

const TOOLTIP_CONDITIONAL_MODIFIER = new vscode.MarkdownString(
	'**Conditional Modifier**: A list property modifier. The property value will be set to the new values.\n\n' +
	'**Example**: \n' +
	'```\n' +
	'stats =\n' +
	'[\n' +
	'    ?(name == Damage) {\n' +
	'        type InstancedItemStat\n' +
	'        min = 22\n' +
	'        max = 50\n' +
	'    }\n' +
	']\n' +
	'```\n'
)

const TOOLTIP_CONDITIONAL_REMOVAL = new vscode.MarkdownString(
	'**Conditional Removal**: A list property modifier. The property will be removed and replaced with the new values.\n\n' +
	'**Example**: \n' +
	'```\n' +
	'stats =\n' +
	'[\n' +
	'    -(name == FoodPerDay)\n' +
	'    {\n' +
	'        id Fantasy.Creatures.Cyclops.FoodPerDay\n' +
	'        type StatType\n' +
	'        inherit Oct.Characters.Stats.FoodPerDay\n' +
	'        initial = -10\n' +
	'    }\n' +
	']\n' +
	'```\n\n' +
	'**Tip**: Use for removing/replacing obsolete or conflicting properties.'
)

const tooltips = new Map<string | RegExp, vscode.MarkdownString>([
	['id', TOOLTIP_ID],
	['type', TOOLTIP_TYPE],
	['alias', TOOLTIP_ALIAS],
	['inherit', TOOLTIP_INHERIT],
	['appendonly', TOOLTIP_APPENDONLY],
	['abstract', TOOLTIP_ABSTRACT],
	['$clear', TOOLTIP_CLEAR],
	[new RegExp('\\?\\([A-Za-z0-9]+\\s+==\\s+[A-Za-z0-9]+\\)'), TOOLTIP_CONDITIONAL_MODIFIER],
	[new RegExp('\\-\\([A-Za-z0-9]+\\s+==\\s+[A-Za-z0-9]+\\)'), TOOLTIP_CONDITIONAL_REMOVAL]
])

const tooltipCache = new Map<string, vscode.MarkdownString | null>()

export function getTooltip(word: string): vscode.MarkdownString | null {
	if (tooltipCache.has(word)) {
		return tooltipCache.get(word) || null
	}

	if (tooltips.has(word)) {
		const result = tooltips.get(word) || null
		tooltipCache.set(word, result)
		return result
	}

	for (const [pattern, value] of tooltips) {
		if (new RegExp(pattern).test(word)) {
			tooltipCache.set(word, value)
			return value
		}
	}

	tooltipCache.set(word, null)
	return null
}