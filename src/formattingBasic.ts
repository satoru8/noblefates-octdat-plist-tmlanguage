import * as vscode from 'vscode'

function formatOctDatBasic(document: vscode.TextDocument, range: vscode.Range) {
	const text = document.getText(range)
	const formattedText = indentOctDat(text)
	return [vscode.TextEdit.replace(range, formattedText)]
}

function indentOctDat(text: string) {
	const lines = text.split('\n')
	let currentIndentation = 0
	let insideObject = false
	let insideArray = false

	const getIndentation = () => '\t'.repeat(currentIndentation)

	const indentedLines = lines.map((line) => {
		const trimmedLine = line.trim()

		if (trimmedLine.startsWith('//')) {
			return line
		}

		const customVariablePattern = /\{[^\}]*\}/
		if (customVariablePattern.test(trimmedLine)) {
			return getIndentation() + trimmedLine
		}

		if (/{[^{}]*}/.test(trimmedLine)) {
			return getIndentation() + trimmedLine
		}

		if (trimmedLine.endsWith('{') || trimmedLine.endsWith('[')) {
			insideObject = true
			insideArray = trimmedLine.endsWith('[')
			const indentedLine = getIndentation() + trimmedLine
			currentIndentation++
			return indentedLine
		}

		if (trimmedLine.startsWith('}') || trimmedLine.startsWith(']')) {
			insideObject = false
			insideArray = trimmedLine.startsWith(']')
			currentIndentation = Math.max(currentIndentation - 1, 0)
			return getIndentation() + trimmedLine
		}

		if (insideObject || insideArray) {
			return getIndentation() + trimmedLine
		}

		return getIndentation() + trimmedLine
	})

	return indentedLines.join('\n')
}

export { formatOctDatBasic }
