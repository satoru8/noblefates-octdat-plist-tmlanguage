import * as vscode from 'vscode'
import { ExtensionSettings } from './settings'

function formatOctDat(document: vscode.TextDocument, range: vscode.Range) {
	const indentationSetting: 'Tab (\\t)' | 'Two Spaces' = ExtensionSettings.get('indentation', 'Tab (\\t)')
	const indentation = indentationSetting === 'Tab (\\t)' ? '\t' : indentationSetting === 'Two Spaces' ? '  ' : '    '
	const text = document.getText(range)
	const formattedText = indentOctDat(text, indentation)
	return [vscode.TextEdit.replace(range, formattedText)]
}

function indentOctDat(text: string, indentString: string) {
	const lines = text.split('\n')
	let currentIndentation = 0

	const getIndentation = () => indentString.repeat(currentIndentation)

	const indentedLines = lines.map((line) => {
		const trimmedLine = line.trim()

		if (trimmedLine.startsWith('//')) {
			return line
		}

		if (trimmedLine.endsWith('{') || trimmedLine.endsWith('[')) {
			const indentedLine = getIndentation() + trimmedLine
			currentIndentation++
			return indentedLine
		}

		if (trimmedLine.startsWith('}') || trimmedLine.startsWith(']')) {
			currentIndentation = Math.max(currentIndentation - 1, 0)
			return getIndentation() + trimmedLine
		}

		if (/^\s*[^\s=]+\s*=\s*\[.*?\]\s*$/.test(trimmedLine) || /^\s*[^\s=]+\s*=\s*\{.*?\}\s*$/.test(trimmedLine)) {
			return getIndentation() + trimmedLine
		}

		if (/^\s*[^\s=]+\s*=\s*[\[{]/.test(trimmedLine)) {
			const indentedLine = getIndentation() + trimmedLine
			return indentedLine
		}

		if (/^\s*\[.*\]$/.test(trimmedLine) || /{[^{}]*}/.test(trimmedLine)) {
			return getIndentation() + trimmedLine
		}

		if (/^\s*\w+\s*=\s*\[/.test(trimmedLine)) {
			const match = trimmedLine.match(/(\s*\w+\s*=\s*)\[.*\]/)
			if (match) {
				currentIndentation++
				return getIndentation() + trimmedLine.replace('[', `\n${getIndentation()}[`)
			}
		}

		return insideObjectOrArray(trimmedLine)
			? indentObjectLine(trimmedLine, currentIndentation, indentString)
			: getIndentation() + trimmedLine
	})

	return indentedLines.join('\n')
}

function insideObjectOrArray(line: string): boolean {
	return line.includes('{') || line.includes('}')
}

function indentObjectLine(line: string, indentation: number, indentString: string) {
	if (/{[^{}]*}/.test(line)) {
		return indentString.repeat(indentation) + line.trim()
	}

	if (line.includes('{') && line.includes('}')) {
		return line
			.split(/([{}])/)
			.map((text) =>
				text === '{' || text === '}'
					? `\n${indentString.repeat(indentation)}${text}\n`
					: indentString.repeat(indentation) + text.trim()
			)
			.join('')
	}

	return indentString.repeat(indentation) + formatObjectProperty(line)
}

function formatObjectProperty(line: string) {
	const [key, value] = line.split('=').map((part) => part.trim())
	return key && value ? `${key} = ${value}` : line
}

export { formatOctDat }
