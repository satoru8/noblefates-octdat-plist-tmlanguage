# Noble Fates OctDat Support

<br>

## Features: 
- Noble Fates Custom File Icon.
- Context menu items for formatting.
- Octdat formatter. Default Format Document with: `Shift + Alt + F`
- Backup `Basic Format Octdat` command/content menu.
- `Format .octdat Files in Directory` command/content menu. (Recursively)
- `Type` IntelliSense.
- Hover Tooltips.
- Create `OctMod.info` command.
- Auto closing & surrounding brackets.
- Custom Grammar. Color support specifc to octdats.
- Snippets to help your workflow. These have many tab options and mirroring.
- .octdat.bak optional extension usage
- Syntax Highlighting is based on VSC Dark+ theme.
- Extension settings.

<br>

## Snippets:

<img src="https://spicykatsu.dev/nebulavault/nfoctpreview.gif" alt="Preview" width="500"/>

<br>

## Known Issues & Requests

Probably? Let me know if so.