import * as fs from 'fs/promises'
import * as path from 'path'

const octdatFolder = 'C:/Program Files (x86)/Steam/steamapps/common/Noble Fates/Noble Fates_Data/StreamingAssets/OctDats'
const outputFile = './dist/json/types.json'
const minify = true
const showCounts = false

const scanFolder = async (folder: string): Promise<string[]> => {
	let files: string[] = []
	try {
		const entries = await fs.readdir(folder, { withFileTypes: true })
		for (const entry of entries) {
			const fullPath = path.join(folder, entry.name)
			if (entry.isDirectory()) {
				files = files.concat(await scanFolder(fullPath))
			} else if (entry.isFile() && fullPath.endsWith('.octdat')) {
				files.push(fullPath)
			}
		}
	} catch (error) {
		console.error(`Error scanning folder ${folder}:`, error)
	}
	return files
}

const extractTypes = async (file: string): Promise<string[]> => {
	const types: string[] = []
	try {
		const fileContent = await fs.readFile(file, 'utf-8')
		const typeRegex = /type\s+([A-Z]\w*)/g
		let match
		while ((match = typeRegex.exec(fileContent)) !== null) {
			types.push(match[1])
		}
	} catch (error) {
		console.error(`Error reading file ${file}:`, error)
	}
	return types
}

const countTypes = (types: string[]): Map<string, number> => {
	const typeCount = new Map<string, number>()
	types.forEach((type) => {
		typeCount.set(type, (typeCount.get(type) || 0) + 1)
	})
	return typeCount
}

const updateTypesFile = async () => {
	if (!fs.access(octdatFolder)) {
		console.error(`Folder ${octdatFolder} does not exist.`)
		process.exit(1)
	}

	try {
		const octdatFiles = await scanFolder(octdatFolder)
		let allTypes: string[] = []

		for (const file of octdatFiles) {
			const types = await extractTypes(file)
			allTypes = allTypes.concat(types)
		}

		const typeCountMap = countTypes(allTypes)
		const sortedTypes = Array.from(typeCountMap.keys()).sort()

		let output: Record<string, number> | string[] = {}

		if (showCounts) {
			output = sortedTypes.reduce(
				(acc, type) => {
					acc[type] = typeCountMap.get(type) || 0
					return acc
				},
				{} as Record<string, number>
			)
		} else {
			output = sortedTypes
		}

		const newFileContent = JSON.stringify(output, null, minify ? 0 : 2)

		try {
			const existingFileContent = await fs.readFile(outputFile, 'utf-8')
			if (existingFileContent === newFileContent) {
				console.log('Types already up to date.')
				return
			}
		} catch (error) {
			if (error.code !== 'ENOENT') {
				throw error
			}
		}

		console.log('Types are out of date. Regenerating...')
		await fs.writeFile(outputFile, newFileContent)
		console.log(`Found ${sortedTypes.length} unique types. Types saved to ${outputFile}.`)
	} catch (error) {
		console.error('Error processing files:', error)
	}
}

updateTypesFile()
